import {Component, OnInit} from '@angular/core';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {LatLngLiteral, MouseEvent} from '@agm/core';
import {Observable} from 'rxjs';
import {ApiaryService} from 'src/app/services/apiary.service';
import {Apiary, Hive} from 'src/app/interfaces/interfaces';
import {ActivatedRoute} from '@angular/router';
import {AlertController} from '@ionic/angular';

@Component({
    selector: 'app-maps',
    templateUrl: './maps.page.html',
    styleUrls: ['./maps.page.scss'],
})
export class MapsPage implements OnInit {

    apiaryID;
    apiaries: Apiary = {address: '', code: '', hives: undefined, id: '', latitud: '', longitud: '', name: ''};
    apiary: Observable<Apiary []>;
    lat: number;
    lng: number;
    zoom = 15;
    private myobj: any;
    private test: any;
    specificMarker = false;

    constructor(private apiaryService: ApiaryService,
                private routeParam: ActivatedRoute,
                public alertCtrl: AlertController,
                private geolocation: Geolocation) { }

    ngOnInit() {
        this.apiaryID = this.routeParam.snapshot.paramMap.get('apiaryID');
        if (this.apiaryID !== null) {
            this.apiaryService.getApiary().subscribe((data: any) => {
                this.apiaries = data.filter((item) => item.id === this.apiaryID)[0];
                this.myobj = JSON.stringify(this.apiaries);
                this.test = JSON.parse(this.myobj);
                this.lat = Number(this.test.latitude);
                console.log('aqui lat', this.lat);
                this.lng = Number(this.test.longitude);
                console.log('aqui lng', this.lat);
            });
        } else {
            this.geolocation.getCurrentPosition().then((resp) => {
                this.lat = resp.coords.latitude;
                this.lng = resp.coords.longitude;
            }).catch((error) => {
                console.log('Error getting location', error);
                this.lat = 0.00;
                this.lng = 0.00;
            });
        }
        this.generateMarker();
    }
    changePickupMarkerLocation($event: { coords: LatLngLiteral}) {
        if (this.specificMarker){
            this.lat = $event.coords.lat;
            this.lng = $event.coords.lng;
        }
    }

    generateMarker() {
        this.apiary = this.apiaryService.getApiary();
    }

    async submitForm() {
        const alert = await this.alertCtrl.create({
            subHeader: 'Guardar nueva ubicacion',
            message: '¿Desea guardar la nueva ubicacion de la colmena?',
            buttons: [
                {
                    text: 'Descartar',
                    cssClass: 'secondary'
                }, {
                    text: 'Guardar'
                }
            ]
        });
        await alert.present();
    }

    changeValue() {
        if (this.specificMarker === false)
            this.specificMarker = true;
        else {
            this.submitForm();
        }
    }

    // clickedMarker(label: string, index: number) {
    //     console.log(`clicked the marker: ${label || index}`);
    // }
    // markerDragEnd(m: marker, $event: MouseEvent) {
    //     console.log('dragEnd', m, $event);
    // }

}

// interface marker {
//     lat: number;
//     lng: number;
//     label?: string;
//     draggable: boolean;
// }
