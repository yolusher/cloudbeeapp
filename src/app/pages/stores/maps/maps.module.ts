import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MapsPage } from './maps.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { AgmCoreModule } from '@agm/core';

const routes: Routes = [
  {
    path: '',
    component: MapsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule,
    AgmCoreModule.forRoot({
      // please get your own API key here:
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en
      apiKey: 'AIzaSyBad93y39OWeaVUhMuLIMqlmgEnxeD64fA',
      libraries: ["places"]
    })
    /*AgmCoreModule.forRoot({
      api18.217.80.128: 'AIzaSyBad93y39OWeaVUhMuLIMqlmgEnxeD64fA'
    })*/
  ],
  declarations: [MapsPage]
})
export class MapsPageModule {}
