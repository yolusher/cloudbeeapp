import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-syncup',
  templateUrl: './syncup.page.html',
  styleUrls: ['./syncup.page.scss'],
})
export class SyncupPage implements OnInit {
  syncmaster;
  syncdocs;
  isIndeterminate:boolean;
  masterCheck:boolean;
  constructor() {
    this.syncmaster=[
      {name : "Colmenas", select : false},
      {name : "Apiarios", select : false},
      {name : "Formulario", select : false}
    ];
    this.syncdocs=[
      {name : "Inspecciones Realizadas", select : false}
    ];
   }

  ngOnInit() {
  }
}
