import { Component, OnInit } from '@angular/core';
import { MenuController, ModalController } from '@ionic/angular';
import { MenuOpt } from 'src/app/interfaces/interfaces';
import { Observable, Subscriber } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ApiaryService } from 'src/app/services/apiary.service';
import { FilterPage } from './filter/filter.page';
import { OrderPage } from './order/order.page';
import { Storage } from '@ionic/storage';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { AlertController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  //userData
  menuopt: Observable<MenuOpt[]>;
  cartitems;
  viewproduct="list";
  openmenu=false;
  latitude:number;
  longitude:number;
  hives;
  hivesfilter;
  filterby;
  public cards:any;
  public header:any;
  constructor(private menuCtrl: MenuController, 
    private router: Router,
    public http: HttpClient,
    private geolocation:Geolocation,
    private apiaryService: ApiaryService,
    private authService: AuthenticationService,
    public modalCtrl: ModalController,
    private routeParam:ActivatedRoute, 
    private storage: Storage) {
      this.ionViewDidLoad();
     }
  ionViewDidLoad(){
    this.geolocation.getCurrentPosition().then((resp)=>{
      console.log("Latitud",resp.coords.latitude);
      console.log("Longitud",resp.coords.latitude);
      this.latitude=resp.coords.latitude;
      this.longitude=resp.coords.longitude;
    }).catch((error)=>{
      console.log('Error getting location', error);
    });
    console.log('ionViewDidLoad MapPage');
  }
  ngOnInit() {
    this.storage.get('apiary').then((val)=>{
      if(val!=null && val != undefined){
        console.log(JSON.parse(val));
      }
    })
    this.storage.get('hive').then((val)=>{
      if(val!=null && val != undefined){
        console.log(JSON.parse(val));
        this.hives=(JSON.parse(val))
        this.hivesfilter=(JSON.parse(val))
      }
    })
    /*this.apiaryService.getHive().subscribe((data:any)=>{
      this.hives=data
    });*/
  }
  searchTerm : any="";
  setFilteredItems(){
    console.log('fsdfsdf ',this.apiaryService.filterItems(this.searchTerm,this.hivesfilter))
    this.hives=this.apiaryService.filterItems(this.searchTerm,this.hivesfilter)
  }
  opened: boolean = false;
  openLeftMenu() {
    this.openmenu=true;
    this.opened = !this.opened;
  }
  closeLeftMenu() {
    this.openmenu=false;
    this.opened = !this.opened;
  }
  toggleMenu(){
    this.openmenu=false;
    this.opened = !this.opened;
  }
  changeView(){
    if(this.viewproduct=="list"){ this.viewproduct="grid"; }
    else{ this.viewproduct="list"; }
  }
  logoutUser(){
    this.authService.logout();
  }
  async filterBy(){
    const modal = await this.modalCtrl.create({
      component : FilterPage
    });
    await modal.present();
    const filterby = await modal.onDidDismiss();
    console.log(filterby.data);
    this.filterby = filterby.data;
  }
  async orderBy(){
    const modal = await this.modalCtrl.create({
      component : OrderPage
    });
    await modal.present();
    const filterby = await modal.onDidDismiss();
    console.log(filterby.data);
    this.filterby = filterby.data;
  }
  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }
}
