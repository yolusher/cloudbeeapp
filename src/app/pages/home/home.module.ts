import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HomePage } from './home.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { MenuComponent } from 'src/app/components/menu/menu.component';
import { OrderPage } from './order/order.page';
import { OrderPageModule } from './order/order.module';
import { FilterPageModule } from './filter/filter.module';
import { FilterPage } from './filter/filter.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule,
    OrderPageModule,
    FilterPageModule
  ],
  entryComponents:[
    OrderPage,
    FilterPage
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
