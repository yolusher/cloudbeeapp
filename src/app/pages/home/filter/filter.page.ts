import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ApiaryService } from 'src/app/services/apiary.service';
import { HiveService } from 'src/app/services/hive.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.page.html',
  styleUrls: ['./filter.page.scss'],
})
export class FilterPage implements OnInit {
  apiaries:any;
  origin:any;
  typpe:any;
  queen:any;
  
  filterapiary: Array<any>=[];
  filterorigin: Array<any>=[];
  filtertype: Array<any>=[];
  filterqueenrace: Array<any>=[];
  constructor(private modalCtrl: ModalController,
    private ApiaryService: ApiaryService) { 
      this.apiaries=[
        {name : "A001 - Surco", checked : false},
        {name : "A002 - Chaclacayo", checked : false},
        {name : "A003 - Ceande", checked : false},
        {name : "A004 - Chilina", checked : false}
      ];
      this.origin=[
        {name : "Rústica", checked : false},
        {name : "Racional", checked : false}
      ];
      this.typpe=[
        {name : "Layens", checked : false},
        {name : "Langstroth", checked : false}
      ];
      this.queen=[
        {name : "Africana", checked : false},
        {name : "Italiana", checked : false}
      ];
    }

  ngOnInit() {
  }
  closeModal(){
    this.modalCtrl.dismiss();
  }
  selectfilterapiary(ing){
    if(ing.checked === true){
      this.filterapiary.push(ing);
    }else{
      this.filterapiary=this.apiaries.filter((ingr)=>{
        return ingr['checked']===true;
      })
    }
    console.log(this.filterapiary);

  }

  selectfilterorigin(ing){
    if(ing.checked === true){
      this.filterorigin.push(ing);
    }else{
      this.filterorigin=this.origin.filter((ingr)=>{
        return ingr['checked']===true;
      })
    }
    console.log(this.filterorigin);
  }
  selectfiltertype(ing){
    if(ing.checked === true){
      this.filtertype.push(ing);
    }else{
      this.filtertype=this.typpe.filter((ingr)=>{
        return ingr['checked']===true;
      })
    }
    console.log(this.filtertype);
  }
  selectfilterqueen(ing){
    if(ing.checked === true){
      this.filterqueenrace.push(ing);
    }else{
      this.filterqueenrace=this.queen.filter((ingr)=>{
        return ingr['checked']===true;
      })
    }
    console.log(this.filterqueenrace);
  }
}
