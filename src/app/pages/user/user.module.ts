import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { UserPage } from './user.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { ChangepasswordPage } from './changepassword/changepassword.page';
import { ChangepasswordPageModule } from './changepassword/changepassword.module';

const routes: Routes = [
  {
    path: '',
    component: UserPage
  }
];

@NgModule({
  entryComponents:[
    ChangepasswordPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule,
    ChangepasswordPageModule
  ],
  declarations: [UserPage]
})
export class UserPageModule {}
