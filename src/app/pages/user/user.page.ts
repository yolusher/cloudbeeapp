import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { UbicationService } from 'src/app/services/ubication.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ChangepasswordPage } from './changepassword/changepassword.page';
import { Storage } from '@ionic/storage';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit {
  currentUser: any={
    id: 1,
    name: "0",
    username: "",
    email: "",
    password: "",
    company: "",
    active: ""
  };
  currentUserSubscription: Subscription;
  
  constructor(public modalCtrl: ModalController,
    private ubication:UbicationService,
    private authService: AuthenticationService, 
    private storage: Storage) { 
      /*this.currentUserSubscription = this.authService.currentUser.subscribe(user => {
        //this.currentUser = user[0];
        console.log(user)
      });*/
    }

  ngOnInit() {
    this.storage.get('USER_INFO').then((val)=>{
      console.log(val)
      if(val!=null && val != undefined){
        this.currentUser=val[0];
      }
    })
  }
  async changePassword(){
    const modal = await this.modalCtrl.create({
      component : ChangepasswordPage
    });
    await modal.present();
  }
}
