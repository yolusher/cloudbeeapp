import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ForminspectionPage } from './forminspection.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { FormConfigService } from 'src/app/services/form-config.service';
import { DynamicFormModule } from 'src/app/common/forms/dynamic-forms.module';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: ForminspectionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    DynamicFormModule,
    ComponentsModule,
    RouterModule.forChild(routes),
  ],
  declarations: [ForminspectionPage],
	providers: [FormConfigService]
})
export class ForminspectionPageModule {}
