import { Component, OnInit, ɵConsole } from '@angular/core';
import { ControlsService } from 'src/app/common/forms/controls.service';
import { FormConfigService } from 'src/app/services/form-config.service';
import { AlertController, LoadingController,ToastController, NavController } from '@ionic/angular';
import { ControlBase } from 'src/app/common/forms/control-base';
import { FormGroup } from '@angular/forms';
import { map } from "rxjs/operators";
import { ActivatedRoute } from '@angular/router';
import { Hive } from 'src/app/interfaces/interfaces';
import { ApiaryService } from 'src/app/services/apiary.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-forminspection',
  templateUrl: './forminspection.page.html',
  styleUrls: ['./forminspection.page.scss'],
})
export class ForminspectionPage implements OnInit {

  controls: ControlBase<any>[];
	form: FormGroup;
  submitted: any;
  ngformulario;
  hiveID;
  code;
  equipment;
  formdynamic:any=[];
  hives:Hive = {id:'',code:'', name:'', origin:'', type:'', queenrace:'', date_installation:'', codeapiary:'',nameapiary:'', review:false};
  readonly FILENAME= 'form1-conf.json';
  constructor(public configService: FormConfigService,
    public controlsService: ControlsService,
    public alertCtrl: AlertController,
    private routeParam:ActivatedRoute,
    private loadingController: LoadingController, 
    public toastController: ToastController, 
    private apiaryService: ApiaryService, 
    private storage: Storage,
    private navCtrl:NavController) {
      this.hiveID = this.routeParam.snapshot.paramMap.get('hiveID');
      this.form = new FormGroup({});
      this.presentLoading('Cargando...');
     }

  ngOnInit() {
    const today=new Date();
    this.apiaryService.getHive().subscribe((data:any)=>{
      this.hives=data.filter((item) => item.code == this.hiveID)[0];
      console.log(data.filter((item) => item.code == this.hiveID)[0]);
      this.code=today.getFullYear()+''+(today.getMonth()+1)+''+today.getDate()+'-'+today.getHours()+''+today.getMinutes()+''+today.getSeconds()+'-'+this.hives.code;
      this.loadingController.dismiss();
    });
  }
  ionViewWillEnter() {
    /*this.configService.getFormConfig(this.FILENAME).subscribe((data:any) => {
      this.controls = this.controlsService.getControls(data);
    });*/
    let arr
    let temp = []
    let t={}
    let form
    this.storage.get('inspectionform').then((val)=>{
      this.storage.get('equipment').then((v)=>{
        arr=JSON.parse(v)
        arr.forEach(element => {
            if(element.code==this.hives.code){
              t={
                "name":element.name,
                "_number":element._number,
                "honey":element.honey,
                "breeding":element.breeding
              }
              temp.push(t)
            }
        });
        if(val!=null && val != undefined){
          //console.log(JSON.parse(val));
          form=JSON.parse(val)
          form[0].data=temp
          this.controls=this.controlsService.getControls((JSON.parse(val)));
          console.log(this.controls)
        }
      })
      console.log("formulario");
    })
		/*/this.configService.getFormConfig(this.FILENAME).map(res => res.json()).subscribe(data => {
					this.controls = this.controlsService.getControls(data);
				});*/
		/*this.form.valueChanges.subscribe(val => {
      this.submitted = val;
    });*/
      this.submitted = this.formdynamic;
  }
  async submitForm($event) {
    const today=new Date();
    let sendform = {
      "user":"admin",
      "date":today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate(),
      "hour":today.getHours()+':'+today.getMinutes(),
      "hives":this.hives.code,
      "code":this.code,
      "response":this.submitted
    }
    //message: JSON.stringify(this.submitted),
    const alert = await this.alertCtrl.create({
      subHeader: 'Guardar registro',
      message : 'Se ingresara inspección ' + this.code + ' ¿Esta seguro de continuar?',
      buttons: [
        {
        text: 'Descartar',
        cssClass: 'secondary',
        handler: () => {
          console.log('Confirm blah');
        }
      }, {
        text: 'Guardar',
        handler: () => {
          this.presentLoading('Enviando...');
          console.log(JSON.stringify(sendform));
          this.apiaryService.sendHive(JSON.stringify(sendform)).subscribe((data:any)=>{
            if(data.color=='success'){
              this.loadingController.dismiss();
              this.presentToast(data.message)
              this.navCtrl.pop()
            }
            else{
              this.loadingController.dismiss();
              this.presentToast(data.message+'. Volver a Intentarlo')
            }
          });
          console.log('Confirm Okay');
        }
      }
    ]
    });
    await alert.present();
    console.log("Success\n", this.submitted);
  }
  async presentLoading(msg) {
    const loading = await this.loadingController.create({
      message: msg,
      translucent: true,
    });
    return await loading.present();
  }
  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
}
