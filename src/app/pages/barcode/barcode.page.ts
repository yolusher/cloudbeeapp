import { Component, OnInit } from '@angular/core';
import { BarcodeScanner,BarcodeScannerOptions} from '@ionic-native/barcode-scanner/ngx';
import { AlertController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiaryService } from 'src/app/services/apiary.service';

@Component({
  selector: 'app-barcode',
  templateUrl: './barcode.page.html',
  styleUrls: ['./barcode.page.scss'],
})
export class BarcodePage implements OnInit {
  options: BarcodeScannerOptions;
  scanedData:any={};
  apiary;
  constructor(private Scanner: BarcodeScanner,
    public alertController: AlertController,
    private router:Router,
    private loadingController: LoadingController, 
    private ApiaryService:ApiaryService) { 
      this.scan();
    }
  async presentAlert(data) {
      const alert = await this.alertController.create({
        header: data,
        subHeader: 'Lo sentimos, no hemos podido encontrar el producto escaneado',
        buttons: [
          {
            text: 'Escanear otro producto',
            cssClass: 'secondary',
            handler: () => {
              this.scan();
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Salir',
            handler: () => {
              this.router.navigate(['home']);
              console.log('Confirm Okay');
            }
          }
        ]
      });
      await alert.present();
    }
  ngOnInit() {
    this.scan();
  }
  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      translucent: true,
    });
    return await loading.present();
  }
  scan(){
    this.options={
      prompt: "Centre el código QR y evitar mover tu Teléfono"
    }
    this.Scanner.scan(this.options).then((data)=>{
      this.presentLoading();
      this.scanedData=data;
      //this.presentAlert(data.text);
      this.ApiaryService.getHive().subscribe((dat:any)=>{
        this.apiary = dat.filter((item) => item.code == this.scanedData.text)
        if(dat.filter((item) => item.code == this.scanedData.text)[0]!=[]) {
          //this.presentAlert(data.text);
          this.loadingController.dismiss();
          this.router.navigate(['/forminspection',this.scanedData.text]);
        }
        else{
          this.loadingController.dismiss();
          this.presentAlert(this.scanedData.text);
        }
      });
    }, (err) => {
      console.log("Error: ",err);
    })
  }

}
