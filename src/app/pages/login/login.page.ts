import { Component, OnInit } from '@angular/core';
import { ApiaryService } from 'src/app/services/apiary.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { AlertController, LoadingController } from '@ionic/angular';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email:string='';
  password:string='';
  constructor(private authService: AuthenticationService,
    private apiaryService: ApiaryService,
    public alertController: AlertController,
    public loadingController: LoadingController) { }

  ngOnInit() {
  }
  loginUser(){
    if(this.email=='' || this.password==''){
      this.presentAlert('Debe ingresar todos los datos');
    }
    else{
      //this.authService.login(this.email, this.password)
      this.presentLoading();
      this.authService.login(this.email, this.password).pipe(first()).subscribe(
      data => {
        this.authService.getData();
        this.loadingController.dismiss();
      },
      error => {
          alert(error);
          this.loadingController.dismiss();
      });
    }
    this.email='';this.password='';
  }
  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      translucent: true,
    });
    return await loading.present();
  }
  async presentAlert(msg) {
    const alert = await this.alertController.create({
      subHeader: msg,
      buttons: [{
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });
    await alert.present();
  }
}
