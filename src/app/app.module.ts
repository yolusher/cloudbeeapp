import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ComponentsModule } from './components/components.module';
import { HttpClientModule } from '@angular/common/http';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx'

import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { IonicStorageModule } from '@ionic/storage';
import { AuthenticationService } from './services/authentication.service';
import { AuthGuardService } from './services/auth-guard.service';

/*const config = {
    apiKey: "AIzaSyC5lNCRoC-xq87WfUbJopqWojIWX-T1Qi0",
    authDomain: "estilos-fecf7.firebaseapp.com",
    databaseURL: "https://estilos-fecf7.firebaseio.com",
    projectId: "estilos-fecf7",
    storageBucket: "estilos-fecf7.appspot.com",
    messagingSenderId: "428822873562",
    appId: "1:428822873562:web:4e3bd6fc1e553f71186ce3",
    measurementId: "G-QV3B0EM2H8"
};*/
const config = {
  apiKey: "AIzaSyAgzkUfpCaVKMvCd1Ob3AxL2eU3Zzr5Dt8",
  authDomain: "estilos-app.firebaseapp.com",
  databaseURL: "https://estilos-app.firebaseio.com",
  projectId: "estilos-app",
  storageBucket: "estilos-app.appspot.com",
  messagingSenderId: "742433223848",
  appId: "1:742433223848:web:7733818f189833e543be9d",
  measurementId: "G-PSVM3N1JWN"
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot({
      menuType: 'overlay'
   }), 
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    ComponentsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(config),
    AngularFireAuthModule,
    AngularFirestoreModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    BarcodeScanner,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    InAppBrowser,
    AuthGuardService,
    AuthenticationService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
