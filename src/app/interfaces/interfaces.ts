export interface MenuOpt {
    icon: string;
    name: string;
    redirectTo: string;
}

export interface Apiary {
    id: string;
    code: string;
    name: string;
    address: string;
    latitud: string;
    longitud: string;
    hives: any;
}

export interface Hive {
    id: string;
    code: string;
    name: string;
    origin: string;
    type: string;
    queenrace: string;
    date_installation: string;
    codeapiary: string;
    nameapiary: string;
    review: boolean;
}
