export interface Department{
    id: string;
    name: string;
}
export interface City{
    id: string;
    department: string;
    name: string;
}
export interface District{
    id: string;
    department: string;
    city: string;
    name: string;
}
export interface Stores{
    id: string;
    city: string;
    name: string;
    address: string;
    latitude: string;
    longitude: string;
    hours: any;
    image: string;
}