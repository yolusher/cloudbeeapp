import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthGuardService} from './services/auth-guard.service';

const routes: Routes = [
    {path: '', redirectTo: 'login', pathMatch: 'full'},
    {path: 'home', loadChildren: './pages/home/home.module#HomePageModule', canActivate: [AuthGuardService]},
    {path: 'login', loadChildren: './pages/login/login.module#LoginPageModule'},
    {path: 'maps', loadChildren: './pages/stores/maps/maps.module#MapsPageModule'},
    {path: 'maps/:apiaryID', loadChildren: './pages/stores/maps/maps.module#MapsPageModule'},
    {path: 'barcode', loadChildren: './pages/barcode/barcode.module#BarcodePageModule'},
    {path: 'user', loadChildren: './pages/user/user.module#UserPageModule'},
    // { path: 'changepassword', loadChildren: './pages/user/changepassword/changepassword.module#ChangepasswordPageModule' },
    {path: 'syncup', loadChildren: './pages/syncup/syncup.module#SyncupPageModule'},
    {path: 'forminspection', loadChildren: './pages/forminspection/forminspection.module#ForminspectionPageModule'},
    {path: 'forminspection/:hiveID', loadChildren: './pages/forminspection/forminspection.module#ForminspectionPageModule'},
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
