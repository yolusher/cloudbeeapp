import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Hive } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class HiveService {

  constructor(private http:HttpClient) { }
  getHive(){
    return this.http.get<Hive[]>('/assets/data/hives.json');
  }
  getHiveId(apiary_id){
    return this.http.get<Hive[]>('/assets/data/hives.json');
  }
}
