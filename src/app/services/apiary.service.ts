import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Apiary, Hive} from '../interfaces/interfaces';
import {Storage} from '@ionic/storage';

@Injectable({
    providedIn: 'root'
})
export class ApiaryService {
    apiary: any = [];
    company=localStorage.getItem('USER_COMPANY')
    constructor(
        private http: HttpClient,
        private storage: Storage) {
        
    }

    getApiary() {
        return this.http.get<Apiary[]>('http://18.221.50.236:443/api/1/getapiaryapp',{ headers: new HttpHeaders({'Content-Type': 'application/json','X-Requested-With': 'XMLHttpRequest'})});
    }

    getApiaryId(apiaryId) {
        return this.http.get<Apiary[]>('http://18.221.50.236:443/api/1/getapiaryapp');
    }

    getHive() {
        return this.http.get<Hive[]>('http://18.221.50.236:443/api/1/gethivesapp',{ headers: new HttpHeaders({'Content-Type': 'application/json','X-Requested-With': 'XMLHttpRequest'})});
    }
    sendHive(form){
        return this.http.post('http://18.221.50.236:443/api/1/result',form,{ headers: new HttpHeaders({'Content-Type': 'application/json','X-Requested-With': 'XMLHttpRequest'})});
    }
    jsonData
    filterI(searchTerm){
        /*this.storage.get('hive').then((val)=>{
            if(val!=null && val != undefined){
              console.log(JSON.parse(val));
              this.jsonData=(JSON.parse(val))
              return this.jsonData.filter((item) => {
                  return item.code.toLowerCase().includes(searchTerm.toLowerCase()) ||  item.name.toLowerCase().includes(searchTerm.toLowerCase()) ||  item.origin.toLowerCase().includes(searchTerm.toLowerCase()) ||  item.nameapiary.toLowerCase().includes(searchTerm.toLowerCase()) ||  item.codeapiary.toLowerCase().includes(searchTerm.toLowerCase());
              }); 
            }
        })*/ 
        this.storage.get('hive').then((val)=>{
            if(val!=null && val != undefined){
              console.log(JSON.parse(val));
              this.jsonData=(JSON.parse(val))
            }
        })
        //console.log('654645',this.filterI(searchTerm,this.jsonData))
        //return this.filterI(searchTerm,this.jsonData)
    }
    filterItems(searchTerm,jd){
        /*console.log('DSFSD',jd.filter((item) => {
            return item.name.toLowerCase().includes(searchTerm.toLowerCase()) ||  item.code.toLowerCase().includes(searchTerm.toLowerCase()) ||  item.nameapiary.toLowerCase().includes(searchTerm.toLowerCase()) ||  item.codeapiary.toLowerCase().includes(searchTerm.toLowerCase());
        }))*/
        return jd.filter((item) => {
            return item.name.toLowerCase().includes(searchTerm.toLowerCase()) ||  item.code.toLowerCase().includes(searchTerm.toLowerCase());
        });  
    }
}

