import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { City, Department, District, Stores } from '../interfaces/ubication';

@Injectable({
  providedIn: 'root'
})
export class UbicationService {

  constructor(private http:HttpClient) { }

  getDepartment(){
    //return this.http.get<Department[]>('/assets/data/department.json');
    return this.http.get<Department[]>('https://www.estilos.com.pe/module/estilosstock/appestilos?method=department',{ headers: new HttpHeaders({'Content-Type': 'application/json','X-Requested-With': 'XMLHttpRequest'})});
  }
  getCity(){
    //return this.http.get<City[]>('/assets/data/cities.json');
    return this.http.get<City[]>('https://www.estilos.com.pe/module/estilosstock/appestilos?method=province',{ headers: new HttpHeaders({'Content-Type': 'application/json','X-Requested-With': 'XMLHttpRequest'})});
  }
  getDistrict(){
    //return this.http.get<District[]>('/assets/data/district.json');
    return this.http.get<District[]>('https://www.estilos.com.pe/module/estilosstock/appestilos?method=district',{ headers: new HttpHeaders({'Content-Type': 'application/json','X-Requested-With': 'XMLHttpRequest'})});
  }
  getStores(){
    //return this.http.get<Stores[]>('/assets/data/stores.json');
    return this.http.get<Stores[]>('https://www.estilos.com.pe/module/estilosstock/appestilos?method=store',{ headers: new HttpHeaders({'Content-Type': 'application/json','X-Requested-With': 'XMLHttpRequest'})});
  }
}
