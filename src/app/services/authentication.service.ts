import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Platform, ToastController, LoadingController } from '@ionic/angular';
import { ApiaryService } from './apiary.service';
import { Apiary } from '../interfaces/interfaces';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  urlreq = 'http://18.221.50.236:443';
  authState = new BehaviorSubject(false);
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor(private router: Router,
    private storage: Storage,
    private platform: Platform,
    public toastController: ToastController, 
    public loadingController: LoadingController,
    private apiaryService: ApiaryService,
    private http:HttpClient) { 
      this.platform.ready().then(() => {
        this.ifLoggedIn();
      });
      this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('USER_DATA')));
      this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): any {
      return this.currentUserSubject.value;
    
    }
    login(email, password) {
      return this.http.post<any>(this.urlreq + '/api/userapp/login',{"username":email,"password":password}).pipe(map((user: any) => {
        if (user) {
          this.storage.set('USER_INFO', user).then((response) => {
            localStorage.setItem('USER_INFO', JSON.stringify(user[0]));
            localStorage.setItem('USER_COMPANY', JSON.stringify(user[0].company));
            this.currentUserSubject.next(user);
          });
        }
        else{
          this.loadingController.dismiss();
          this.presentToast('Usuario y/o contraseña incorrecta')
        }
        return user;
      }));
      /*this.presentLoading();
      var dummy_response = {
        user_id: '007',
        user_name: 'Admin CloudBee',
        user_username: 'admin',
        user_email: 'admin@cloudbee.com',
        user_password: 'terminal'
      };
      if(email==dummy_response.user_username && password==dummy_response.user_password){
        this.storage.set('USER_INFO', dummy_response).then((response) => {
          
        });
      }
      else{
        this.loadingController.dismiss();
        this.presentToast('Usuario y/o contraseña incorrecta')
      }*/
    }
    logout() {
      this.storage.remove('USER_INFO').then(() => {
        this.storage.remove('apiary');
        this.storage.remove('hive');
        this.router.navigate(['login']);
        this.authState.next(false);
      });
    }
    getData(){
      let data = this.http.get<Apiary[]>(this.urlreq+'/api/1/getapiaryapp',{ headers: new HttpHeaders({'Content-Type': 'application/json','X-Requested-With': 'XMLHttpRequest'})});
      let datah = this.http.get<Apiary[]>(this.urlreq+'/api/1/gethivesapp',{ headers: new HttpHeaders({'Content-Type': 'application/json','X-Requested-With': 'XMLHttpRequest'})});
      let insp = this.http.get(this.urlreq+'/api/1/getform',{ headers: new HttpHeaders({'Content-Type': 'application/json','X-Requested-With': 'XMLHttpRequest'})});
      let equip = this.http.get(this.urlreq+'/api/1/equipment',{ headers: new HttpHeaders({'Content-Type': 'application/json','X-Requested-With': 'XMLHttpRequest'})});
      data.subscribe(result =>{
        this.storage.set('apiary', JSON.stringify(result));
        datah.subscribe(resulth =>{
          this.storage.set('hive', JSON.stringify(resulth));
          insp.subscribe(resulti =>{
            this.storage.set('inspectionform', JSON.stringify(resulti));
            equip.subscribe(resul =>{console.log('fsdfds'+JSON.stringify(resul));this.storage.set('equipment', JSON.stringify(resul));})
            this.loadingController.dismiss();
            this.presentToast('Sesion iniciada correctamente');
            this.router.navigate(['home']);
            this.authState.next(true);
          });
        });
      });
    }
    isAuthenticated() {
      return this.authState.value;
    }
    ifLoggedIn() {
      this.storage.get('USER_INFO').then((response) => {
        if (response) {
          this.authState.next(true);
        }
      });
    }
    async presentLoading() {
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        translucent: true,
      });
      return await loading.present();
    }
    async presentToast(msg) {
      const toast = await this.toastController.create({
        message: msg,
        duration: 2000
      });
      toast.present();
    }
}
