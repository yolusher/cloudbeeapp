import { ControlBase } from './control-base';

export class TableControl extends ControlBase<string> {
  controlType = 'table';
  options: { key: string, value: string }[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['data'] || [];
  }
}