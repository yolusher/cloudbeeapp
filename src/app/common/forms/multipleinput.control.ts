import { ControlBase } from './control-base';

export class MultipleInput extends ControlBase<string> {
  controlType = 'multipleinput';
  options: { key: string, value: string }[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['data'] || [];
  }
}