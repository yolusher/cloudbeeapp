import { Injectable } from '@angular/core';
import { ControlDescriptor } from './control';
import { SelectControl } from './select.control';
import { TextboxControl } from './textbox.control';
import { TextareaControl } from './textarea.control';
import { CheckboxControl } from './checkbox.control';
import { MultipleInput } from './multipleinput.control';
import { TableControl } from './table.control';
import { MultipleSelect } from './multipleselect.control';

@Injectable({
  providedIn: 'root'
})
export class ControlsService {

  constructor() { }
  getControls(descriptors: ControlDescriptor[]) {
    let controls = descriptors.map((descriptor, index) => {
      let options = {
        ...descriptor,
        type: descriptor.type,
        key: descriptor.name,
        label: descriptor.title,
        value: '',
        order: index
      };

      switch (descriptor.type) {
        case 'text':
        case 'number':
          return new TextboxControl(options);
        case 'textarea':
          return new TextareaControl(options);
        case 'select':
          return new SelectControl(options);
        case 'checkbox':
          return new CheckboxControl(options);
        case 'multipleinput':
          return new MultipleInput(options);
        case 'table':
          return new TableControl(options);
        case 'multipleselect':
          return new MultipleSelect(options);
        default:
          console.error(`${descriptor.type} is not supported`);}
    });

    return controls.filter(x => !!x).sort((a, b) => a.order - b.order);
  }
}
