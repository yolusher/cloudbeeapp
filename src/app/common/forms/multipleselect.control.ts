import { ControlBase } from './control-base';

export class MultipleSelect extends ControlBase<string> {
  controlType = 'multipleselect';
  options: { key: string, value: string }[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['data'] || [];
  }
}