import { NgModule } from '@angular/core';
import { DynamicControlsService } from './dynamic-controls.service';
import { ControlsService } from './controls.service';
import { DynamicFormComponent } from './dynamic-forms.component';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';  

@NgModule({
  imports: [CommonModule,IonicModule, FormsModule,ReactiveFormsModule],
  providers: [DynamicControlsService, ControlsService],
  declarations: [DynamicFormComponent],
  exports: [DynamicFormComponent]
})
export class DynamicFormModule { }
