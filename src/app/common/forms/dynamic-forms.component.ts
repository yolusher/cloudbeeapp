import { Component, OnInit, Input, Output, EventEmitter, NgModule } from '@angular/core';
import { ControlBase } from './control-base';
import { FormGroup } from '@angular/forms';
import { DynamicControlsService } from './dynamic-controls.service';
@Component({
  selector: 'dynamic-form',
  templateUrl: './dynamic-forms.component.html',
  styleUrls: ['./dynamic-forms.component.scss'],
  exportAs: 'dynamicForm'
})
export class DynamicFormComponent implements OnInit {
  @Input() controls: ControlBase<any>[] = [];
  @Input() form: FormGroup;
  @Input() formdynamic:any=[];
  @Output() submit: EventEmitter<any> = new EventEmitter();

  //formdynamic:any=[];
  subadd:any=[];
  constructor(private controlsService: DynamicControlsService) {
   }

  ngOnInit() {
    this.form = this.controlsService.toFormGroup(this.form, this.controls);
    this.controls.forEach((catData: any) => {
      if(catData.controlType=='table'){
        this.subadd=[];
        catData.options.forEach((inp: any) => {
          this.subadd.push({"name":inp.name,"_number":"","honey":"","breeding":""});
        });
        this.formdynamic.push({"name":catData.label,"data":this.subadd});
      }
      if(catData.controlType=='multipleinput'){
        this.subadd=[];
        console.log(catData)
        catData.options.forEach((inp: any) => {
          this.subadd.push({"name":inp.label,"data":"","um":inp.um});
        });
        this.formdynamic.push({"name":catData.label,"data":this.subadd});
      }
      if(catData.controlType!='multipleinput' && catData.controlType!='table' && catData.controlType!='multipleselect'){
        this.formdynamic.push({"name":catData.label,"data":""});
      }
      if(catData.controlType=='multipleselect'){
        this.subadd=[];
        
        catData.options.forEach((inp: any) => {
          if(inp.hasOwnProperty('treatment')){
            let sudtmt = [];
            inp.treatment.forEach((tmt: any) => {
              console.log(inp)
              sudtmt.push({"name":tmt.name,"data":"","um":tmt.um});
            });
            this.subadd.push({"name":inp.name,"data":sudtmt});
          }
          if(inp.hasOwnProperty('task')){
            let sudtmt = [];
            inp.task.forEach((tmt: any) => {
              console.log(inp)
              sudtmt.push({"name":tmt.name,"data":""});
            });
            this.subadd.push({"name":inp.name,"data":sudtmt});
          }
          /*inp.treatment.forEach((tmt: any) => {
            sudtmt.push({"name":tmt.name,"data":"","um":tmt.um});
          });
          this.subadd.push({"name":inp.name,"data":sudtmt});*/
        });
        this.formdynamic.push({"name":catData.label,"data":this.subadd});
      }
    });
    console.log(this.formdynamic)
  }
  onSubmit(formdynamic) {
    console.log(formdynamic)
    this.submit.next(this.form.value);
  }
  visibleIndex = -1;
  toggleFunc(ind) {
    if (this.visibleIndex === ind) {
      this.visibleIndex = -1;
    } else {
      this.visibleIndex = ind;
    }
  }
}
