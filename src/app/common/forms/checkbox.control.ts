import { ControlBase } from './control-base';

export class CheckboxControl extends ControlBase<string> {
  controlType = 'checkbox';
  options: { key: string, value: string }[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || [];
  }
}