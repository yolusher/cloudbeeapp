import { Component, OnInit, Input } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss'],
})
export class SearchbarComponent implements OnInit {
  @Input () titulo:String;
  openmenu=false;
  cartitems;
  constructor(
    public menuCtrl: MenuController) {
      
     }

  ngOnInit() {
    
  }

  isOpen(){
    this.menuCtrl.isOpen("primerMenu").then(function(value) {
      //console.log(value); // <- your promise data exists here.
      if(value){
        console.log(value);
      }
      else{
        console.log(value);
      }
      //this.openmenu=value;
    });
    //console.log(this.openmenu);*/
    //this.openmenu=true;
  }
  closeMenu() {
    this.menuCtrl.close("primerMenu");
    this.openmenu=false;
    //console.log("aqui")
  }
  /*toggleMenu() {
    this.menuCtrl.toggle("primerMenu"); //Add this method to your button click function
  }*/
  opened: boolean = false;
  openLeftMenu() {
    //document.getElementById("leftMenu").style.display = "block";
    this.openmenu=true;
    this.opened = !this.opened;
  }

  closeLeftMenu() {
    //document.getElementById("leftMenu").style.display = "none";
    this.openmenu=false;
    this.opened = !this.opened;
  }
  toggleMenu(){
    //document.getElementById("leftMenu").style.display = "none"; //ClassName
    this.openmenu=false;
    this.opened = !this.opened;
  }
}
