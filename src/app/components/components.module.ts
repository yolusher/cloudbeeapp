import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { IonicModule } from '@ionic/angular';
import { MenuComponent } from './menu/menu.component';
import { RouterModule } from '@angular/router';
import { SearchbarComponent } from './searchbar/searchbar.component';
import { SearchComponent } from './search/search.component';
import {MapbarComponent} from '../mapbar/mapbar.component';



@NgModule({
  declarations: [
    HeaderComponent,
    MenuComponent,
    SearchbarComponent,
    SearchComponent,
    MapbarComponent
  ],
  exports: [
    HeaderComponent,
    MenuComponent,
    SearchbarComponent,
    SearchComponent,
    MapbarComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule
  ]
})
export class ComponentsModule { }
