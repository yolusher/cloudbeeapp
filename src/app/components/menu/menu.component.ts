import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MenuOpt } from 'src/app/interfaces/interfaces';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  menuopt: Observable<MenuOpt[]>;

  constructor(
    public menuCtrl: MenuController) { }

  ngOnInit() {
  }
  toggleMenu() {
    this.menuCtrl.toggle("primerMenu"); //Add this method to your button click function
  }
  cerrar(){
    this.menuCtrl.close("primerMenu");
  }

}
