import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapbarComponent } from './mapbar.component';

describe('MapbarComponent', () => {
  let component: MapbarComponent;
  let fixture: ComponentFixture<MapbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapbarComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
