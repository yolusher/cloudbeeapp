import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-mapbar',
    templateUrl: './mapbar.component.html',
    styleUrls: ['./mapbar.component.scss'],
})
export class MapbarComponent implements OnInit {
    @Input() titulo: String;

    constructor() {
    }

    ngOnInit() {
    }

}
